to run : uvicorn main:app --app-dir monizberg or directly python monizberg/main.py

to test : install poetry then poetry install & poetry run pytest, it runs on python 3.10.2

docs here : http://127.0.0.1:8000/docs or http://127.0.0.1:8000/redoc

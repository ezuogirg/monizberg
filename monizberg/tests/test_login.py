import pytest
from httpx import AsyncClient, codes

from ..main import app


@pytest.fixture
def good_credentials():
    return ("kim@monster.guru", "#ukraine")


class TestLogin:
    @pytest.mark.asyncio
    @pytest.mark.parametrize(
        "bad_credentials",
        [
            {"username": "username", "password": "password"},
            {"username": "kim@monster.guru", "password": "bad_password"},
        ],
    )
    async def test_login_invalid_credentials_or_password(self, bad_credentials):
        async with AsyncClient(app=app, base_url="http://test") as ac:
            response = await ac.post("/api/login", data=bad_credentials)
        assert response.status_code == codes.UNAUTHORIZED
        assert response.json() == {"detail": "Invalid credentials"}

    @pytest.mark.asyncio
    async def test_login_valid_credentials(self, good_credentials):
        async with AsyncClient(app=app, base_url="http://test") as ac:
            response = await ac.post(
                "/api/login",
                data={"username": good_credentials[0], "password": good_credentials[1]},
            )
        assert response.status_code == codes.OK
        assert list(response.json().keys()) == ["access_token", "token_type"]
        assert response.json()["token_type"] == "bearer"
        pytest.BEARER_TOKEN = response.json()

    @pytest.mark.asyncio
    async def test_user_me_not_authenticated(self):
        async with AsyncClient(app=app, base_url="http://test") as ac:
            response = await ac.get("/api/user/me")
        assert response.status_code == codes.UNAUTHORIZED
        assert response.json() == {"detail": "Not authenticated"}

    @pytest.mark.asyncio
    async def test_user_me_authenticated(self):
        headers = {"Authorization": f'Bearer {pytest.BEARER_TOKEN["access_token"]}'}
        async with AsyncClient(app=app, base_url="http://test") as ac:
            response = await ac.get("/api/user/me", headers=headers)
        assert response.status_code == codes.OK
        assert response.json() == {
            "username": "kim@monster.guru",
            "groups": [],
            "name": "Kim K.",
        }

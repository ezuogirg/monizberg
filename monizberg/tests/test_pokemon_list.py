import pytest
from httpx import AsyncClient, codes

from ..main import DB, app, settings
from .test_login import good_credentials


@pytest.fixture
def define_api_limit():
    # for tests only set limit to 20
    settings.POKEMON_API_LIMIT = 20


class TestPokemonList:
    @pytest.mark.asyncio
    async def test_login_valid_credentials(self, good_credentials):
        async with AsyncClient(app=app, base_url="http://test") as ac:
            response = await ac.post(
                "/api/login",
                data={"username": good_credentials[0], "password": good_credentials[1]},
            )
        assert response.status_code == codes.OK
        assert list(response.json().keys()) == ["access_token", "token_type"]
        assert response.json()["token_type"] == "bearer"
        pytest.BEARER_TOKEN = response.json()

    @pytest.mark.asyncio
    async def test_pokemon_list_not_authenticated(self):

        async with AsyncClient(app=app, base_url="http://test") as ac:
            response = await ac.get("/api/pokemon")
        assert response.status_code == codes.UNAUTHORIZED
        assert response.json() == {"detail": "Not authenticated"}

    @pytest.mark.asyncio
    async def test_pokemon_list_no_group_authenticated(self):
        # reset groups
        DB["users"]["kim@monster.guru"]["groups"] = []

        headers = {"Authorization": f'Bearer {pytest.BEARER_TOKEN["access_token"]}'}
        async with AsyncClient(app=app, base_url="http://test") as ac:
            response = await ac.get("/api/pokemon", headers=headers)
        assert response.status_code == codes.NOT_FOUND
        assert response.json() == {"detail": "group not found"}

    @pytest.mark.asyncio
    async def test_pokemon_list_group_dragon_authenticated(self, define_api_limit):
        add_type = "dragon"

        # reset groups
        DB["users"]["kim@monster.guru"]["groups"] = [add_type]

        headers = {"Authorization": f'Bearer {pytest.BEARER_TOKEN["access_token"]}'}
        async with AsyncClient(app=app, base_url="http://test") as ac:
            response = await ac.get("/api/pokemon", headers=headers)
        assert response.status_code == codes.OK
        assert response.json() == []

    @pytest.mark.asyncio
    async def test_pokemon_list_group_fire_authenticated(self, define_api_limit):
        add_type = "fire"

        # reset groups
        DB["users"]["kim@monster.guru"]["groups"] = [add_type]

        headers = {"Authorization": f'Bearer {pytest.BEARER_TOKEN["access_token"]}'}
        async with AsyncClient(app=app, base_url="http://test") as ac:
            response = await ac.get("/api/pokemon", headers=headers)
        assert response.status_code == codes.OK
        assert response.json() == [
            {"id": 4, "name": "charmander"},
            {"id": 5, "name": "charmeleon"},
            {"id": 6, "name": "charizard"},
        ]

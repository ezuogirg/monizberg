import pytest
from httpx import AsyncClient, codes

from ..main import app
from .test_login import good_credentials


class TestGroup:
    @pytest.mark.asyncio
    async def test_login_valid_credentials(self, good_credentials):
        async with AsyncClient(app=app, base_url="http://test") as ac:
            response = await ac.post(
                "/api/login",
                data={"username": good_credentials[0], "password": good_credentials[1]},
            )
        assert response.status_code == codes.OK
        assert list(response.json().keys()) == ["access_token", "token_type"]
        assert response.json()["token_type"] == "bearer"
        pytest.BEARER_TOKEN = response.json()

    @pytest.mark.asyncio
    async def test_group_add_not_authenticated(self):
        add_type = "normal"

        async with AsyncClient(app=app, base_url="http://test") as ac:
            response = await ac.post(f"/api/group/{add_type}/add")
        assert response.status_code == codes.UNAUTHORIZED
        assert response.json() == {"detail": "Not authenticated"}

    @pytest.mark.asyncio
    async def test_group_add_authenticated(self):
        add_type = "normal"

        headers = {"Authorization": f'Bearer {pytest.BEARER_TOKEN["access_token"]}'}
        async with AsyncClient(app=app, base_url="http://test") as ac:
            response = await ac.post(f"/api/group/{add_type}/add", headers=headers)
        assert response.status_code == codes.OK
        assert response.json() == {
            "username": "kim@monster.guru",
            "groups": [f"{add_type}"],
            "name": "Kim K.",
        }

    @pytest.mark.asyncio
    async def test_group_add_bad_type(self):
        wrong_type = "wrong_type"

        headers = {"Authorization": f'Bearer {pytest.BEARER_TOKEN["access_token"]}'}
        async with AsyncClient(app=app, base_url="http://test") as ac:
            response = await ac.post(f"/api/group/{wrong_type}/add", headers=headers)
        assert response.status_code == codes.UNPROCESSABLE_ENTITY
        assert (
            response.json()["detail"][0]["msg"]
            == "value is not a valid enumeration member; permitted: 'normal', 'fighting', 'flying', 'poison', 'ground', 'rock', 'bug', 'ghost', 'steel', 'fire', 'water', 'grass', 'electric', 'psychic', 'ice', 'dragon', 'dark', 'fairy', 'unknown', 'shadow'"
        )

    @pytest.mark.asyncio
    async def test_group_remove_not_authenticated(self):
        remove_type = "normal"

        async with AsyncClient(app=app, base_url="http://test") as ac:
            response = await ac.post(f"/api/group/{remove_type}/remove")
        assert response.status_code == codes.UNAUTHORIZED
        assert response.json() == {"detail": "Not authenticated"}

    @pytest.mark.asyncio
    async def test_group_remove_authenticated(self):
        remove_type = "normal"

        headers = {"Authorization": f'Bearer {pytest.BEARER_TOKEN["access_token"]}'}
        async with AsyncClient(app=app, base_url="http://test") as ac:
            response = await ac.post(
                f"/api/group/{remove_type}/remove", headers=headers
            )
        assert response.status_code == codes.OK
        assert response.json() == {
            "username": "kim@monster.guru",
            "groups": [],
            "name": "Kim K.",
        }

    @pytest.mark.asyncio
    async def test_group_remove_bad_type(self):
        wrong_type = "wrong_type"

        headers = {"Authorization": f'Bearer {pytest.BEARER_TOKEN["access_token"]}'}
        async with AsyncClient(app=app, base_url="http://test") as ac:
            response = await ac.post(
                f"/api/group/{wrong_type}/remove", headers=headers
            )
        assert response.status_code == codes.UNPROCESSABLE_ENTITY
        assert (
            response.json()["detail"][0]["msg"]
            == "value is not a valid enumeration member; permitted: 'normal', 'fighting', 'flying', 'poison', 'ground', 'rock', 'bug', 'ghost', 'steel', 'fire', 'water', 'grass', 'electric', 'psychic', 'ice', 'dragon', 'dark', 'fairy', 'unknown', 'shadow'"
        )

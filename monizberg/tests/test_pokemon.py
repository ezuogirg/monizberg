import pytest
from httpx import AsyncClient, codes

from ..main import DB, app
from .test_login import good_credentials

POKEMON_ID_EXISTS = (1, "bulbasaur")
POKEMON_ID_NOT_EXISTS = (999, "hellokitty")


class TestPokemonId:
    @pytest.mark.asyncio
    async def test_login_valid_credentials(self, good_credentials):
        async with AsyncClient(app=app, base_url="http://test") as ac:
            response = await ac.post(
                "/api/login",
                data={"username": good_credentials[0], "password": good_credentials[1]},
            )
        assert response.status_code == codes.OK
        assert list(response.json().keys()) == ["access_token", "token_type"]
        assert response.json()["token_type"] == "bearer"
        pytest.BEARER_TOKEN = response.json()

    @pytest.mark.asyncio
    async def test_pokemon_id_not_authenticated(self):
        id = 1

        async with AsyncClient(app=app, base_url="http://test") as ac:
            response = await ac.get(f"/api/pokemon/{id}")
        assert response.status_code == codes.UNAUTHORIZED
        assert response.json() == {"detail": "Not authenticated"}

    @pytest.mark.asyncio
    async def test_pokemon_id_no_group_authenticated(self):
        id = 1

        headers = {"Authorization": f'Bearer {pytest.BEARER_TOKEN["access_token"]}'}
        async with AsyncClient(app=app, base_url="http://test") as ac:
            response = await ac.get(f"/api/pokemon/{id}", headers=headers)
        assert response.status_code == codes.NOT_FOUND
        assert response.json() == {"detail": "group not found"}

    @pytest.mark.asyncio
    @pytest.mark.parametrize("id", POKEMON_ID_EXISTS)
    async def test_pokemon_id_group_normal_authenticated(self, id):
        add_type = "normal"

        # reset groups
        DB["users"]["kim@monster.guru"]["groups"] = [add_type]

        headers = {"Authorization": f'Bearer {pytest.BEARER_TOKEN["access_token"]}'}
        async with AsyncClient(app=app, base_url="http://test") as ac:
            response = await ac.get(f"/api/pokemon/{id}", headers=headers)
        assert response.status_code == codes.NOT_FOUND
        assert response.json() == {"detail": "no pokemon for groups"}

    @pytest.mark.asyncio
    @pytest.mark.parametrize("id", POKEMON_ID_EXISTS)
    async def test_pokemon_id_group_poison_authenticated(self, id):
        add_type = "poison"

        # reset groups
        DB["users"]["kim@monster.guru"]["groups"] = [add_type]

        headers = {"Authorization": f'Bearer {pytest.BEARER_TOKEN["access_token"]}'}
        async with AsyncClient(app=app, base_url="http://test") as ac:
            response = await ac.get(f"/api/pokemon/{id}", headers=headers)
        assert response.status_code == codes.OK
        assert response.json()["types"][1]["type"]["name"] == add_type

    @pytest.mark.asyncio
    @pytest.mark.parametrize("id", POKEMON_ID_NOT_EXISTS)
    async def test_pokemon_unknown_id_group_poison_authenticated(self, id):

        headers = {"Authorization": f'Bearer {pytest.BEARER_TOKEN["access_token"]}'}
        async with AsyncClient(app=app, base_url="http://test") as ac:
            response = await ac.get(f"/api/pokemon/{id}", headers=headers)
        assert response.status_code == codes.NOT_FOUND
        assert response.json() == {"detail": f"no pokemon '{id}' found"}

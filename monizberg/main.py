import time
from enum import Enum
from importlib.metadata import version
from typing import List, Union

import httpx
import uvicorn
from fastapi import Depends, FastAPI, HTTPException, Request
from fastapi.responses import FileResponse
from fastapi.security import OAuth2PasswordRequestForm
from fastapi_login import LoginManager
from fastapi_login.exceptions import InvalidCredentialsException
from pydantic import BaseModel, BaseSettings, EmailStr, HttpUrl

app = FastAPI()


class Settings(BaseSettings):
    SECRET: str
    POKEMON_API_URL: HttpUrl
    POKEMON_API_LIMIT: int = 20

    class Config:
        env_file = ".env"


settings = Settings()

manager = LoginManager(settings.SECRET, "/api/login")


@app.middleware("http")
async def add_process_time_header(request: Request, call_next):
    start_time = time.time()
    response = await call_next(request)
    process_time = time.time() - start_time
    response.headers["X-Process-Time"] = str(process_time)
    response.headers["X-Version"] = version('monizberg')

    return response


DB = {
    "users": {
        "kim@monster.guru": {
            "name": "Kim K.",
            "username": "kim@monster.guru",
            "password": "#ukraine",
            "groups": [],
        }
    }
}


class UserOut(BaseModel):
    username: EmailStr
    name: str
    groups: list[str] = []


class TokenOut(BaseModel):
    access_token: str
    token_type: str


class PokemonOut(BaseModel):
    id: int
    name: str


class TypeEnum(str, Enum):
    # list here https://pokeapi.co/api/v2/type/
    normal = "normal"
    fighting = "fighting"
    flying = "flying"
    poison = "poison"
    ground = "ground"
    rock = "rock"
    bug = "bug"
    ghost = "ghost"
    steel = "steel"
    fire = "fire"
    water = "water"
    grass = "grass"
    electric = "electric"
    psychic = "psychic"
    ice = "ice"
    dragon = "dragon"
    dark = "dark"
    fairy = "fairy"
    unknown = "unknown"
    shadow = "shadow"


@manager.user_loader()
def query_user(user_id: str):
    """
    Get a user from the db
    :param user_id: E-Mail of the user
    :return: None or the user object
    """
    return DB["users"].get(user_id)


@app.post("/api/login", response_model=TokenOut, responses={401: {"model": ""}})
def login(data: OAuth2PasswordRequestForm = Depends()):
    email = data.username
    password = data.password

    user = query_user(email)
    if not user:
        # you can return any response or error of your choice
        raise InvalidCredentialsException
    elif password != user["password"]:
        raise InvalidCredentialsException

    access_token = manager.create_access_token(
        data={"sub": email},
    )
    return {"access_token": access_token, "token_type": "bearer"}


@app.get("/api/user/me", response_model=UserOut, responses={401: {"model": ""}})
def user_details(user=Depends(manager)):
    return user


@app.post(
    "/api/group/{type}/add", response_model=UserOut, responses={401: {"model": ""}}
)
def add_type(type: TypeEnum, user=Depends(manager)):
    if type not in user["groups"]:
        user["groups"].append(type)

    return user


@app.post(
    "/api/group/{type}/remove", response_model=UserOut, responses={401: {"model": ""}}
)
def remove_type(type: TypeEnum, user=Depends(manager)):
    if type in user["groups"]:
        user["groups"].remove(type)

    return user


@app.get(
    "/api/pokemon", response_model=List[PokemonOut], responses={401: {"model": ""}}
)
async def list_pokemon(user=Depends(manager)):

    seen_ids = []

    res = []

    if not user["groups"]:
        raise HTTPException(status_code=httpx.codes.NOT_FOUND, detail="group not found")

    async with httpx.AsyncClient() as client:
        results = await client.get(
            f"{settings.POKEMON_API_URL}/pokemon/?limit={settings.POKEMON_API_LIMIT}"
        )

        if results.status_code != httpx.codes.OK:
            raise HTTPException(
                status_code=httpx.codes.NOT_FOUND, detail=f"no pokemon found"
            )

        for pokemon in results.json()["results"]:
            async with httpx.AsyncClient() as client2:
                results2 = await client2.get(pokemon["url"])

                results2 = results2.json()
                if results2["id"] in seen_ids:
                    continue

                for type in results2["types"]:
                    if type["type"]["name"] in user["groups"]:
                        seen_ids.append(results2["id"])
                        res.append(
                            {
                                "name": results2["name"],
                                "id": results2["id"],
                            }
                        )
                        break

    return res


@app.get("/api/pokemon/{id}", responses={401: {"model": ""}})
async def list_pokemon_id(id: Union[int, str], user=Depends(manager)):
    if not user["groups"]:
        raise HTTPException(status_code=httpx.codes.NOT_FOUND, detail="group not found")

    async with httpx.AsyncClient() as client:
        result = await client.get(f"{settings.POKEMON_API_URL}/pokemon/{id}")

        if result.status_code != httpx.codes.OK:
            raise HTTPException(
                status_code=httpx.codes.NOT_FOUND, detail=f"no pokemon '{id}' found"
            )

        for type in result.json()["types"]:
            if type["type"]["name"] in user["groups"]:
                return result.json()

    raise HTTPException(
        status_code=httpx.codes.NOT_FOUND, detail="no pokemon for groups"
    )


@app.get(
    "/", response_class=FileResponse, responses={200: {"content": {"image/png": {}}}}
)
def root():
    return "images/catch.jpg"


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
